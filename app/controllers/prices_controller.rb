class PricesController < ApplicationController
  protect_from_forgery with: :null_session
  def new
    @price = Price.new
  end

  def create
    @price = Price.new(price_params)
   
    if @price.save
      
      flash[:notice] = "Позиция добавлена"
      redirect_to prices_path
    else
      render :action => 'new'
    end      
  end

  def show
    @price = Price.find(params[:id])
  end

  def index
    @index = 0
    @prices = Price.all
  end

  def edit
    @price = Price.find(params[:id])
  end

  def destroy
    @price = Price.find(params[:id])
    @price.destroy
    flash[:notice] = "Продукт удален."
    redirect_to prices_path
  end

  private
    def price_params
      params.require(:price).permit(:name, :description, :price)
    end
end
