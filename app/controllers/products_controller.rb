class ProductsController < ApplicationController
protect_from_forgery with: :null_session
  def index
  	@products = Product.all
  end

  def new
  	@product = Product.new
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])
    if @product.update_attributes(product_params)
      flash[:notice] = "Продукт обновлен."
      redirect_to product_url
    else
      render :action => 'edit'
    end
  end

  def show
  	@product = Product.find(params[:id])
    
  end

  def create
    @product = Product.new(product_params)
   
    if @product.save
      
      flash[:notice] = "Продукт добавлен"
      redirect_to @product
    else
      render :action => 'new'
    end      
  end

  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    flash[:notice] = "Продукт удален."
    redirect_to products_path
  end
  private
    def product_params
      params.require(:product).permit(:id, :name, :desc, :image, :price)
    end
end