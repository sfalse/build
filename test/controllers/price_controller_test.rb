require 'test_helper'

class PriceControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get price_new_url
    assert_response :success
  end

  test "should get edit" do
    get price_edit_url
    assert_response :success
  end

  test "should get index" do
    get price_index_url
    assert_response :success
  end

  test "should get show" do
    get price_show_url
    assert_response :success
  end

  test "should get _form" do
    get price__form_url
    assert_response :success
  end

end
