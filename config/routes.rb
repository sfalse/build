Rails.application.routes.draw do
  devise_for :users
  get 'persons/profile'

  get 'persons/profile', as: 'user_root'
  resources :prices

  root 'view#General'
  get 'view/General'
  resources :products

  get "view/Contact" => "view#Contact"

  get 'admin/edit'
  
end
